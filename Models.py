class Project:
    def __init__(self, name, description, user_id, members=None, id=None):
        if members is None:
            members = []
        self.name = name
        self.description = description
        self.user_id = user_id
        self.members = members
        self.id = id

class Task:
    def __init__(self, name, desc, project_id, user_id, first_date, second_date, tags, priority, periodical,
                 archive, is_subtask=0, id = 0):
        self.name = name
        self.desc = desc
        self.project_id = project_id
        self.user_id = user_id
        self.first_date = first_date
        self.second_date = second_date
        self.tags = tags
        self.priority = priority
        self.periodical = periodical
        self.archive = archive
        self.is_subtask = is_subtask
        self.id = id

class User:
    def __init__(self, username, password, email, token, user_id=None):
        self.username = username
        self.password = password
        self.email = email
        self.token = token
        self.user_id = user_id

