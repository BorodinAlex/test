import sqlite3

from Models import Project, User

#PROJECT WORK WITH DB
class ProjectStorage:
    @classmethod
    #add project
    def add_project_to_db(cls, project, user):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT name FROM projects")
        all_projectnames = c.fetchall()
        yep = False
        for i in all_projectnames:
            if project.name == i[0]:
                yep = True
        if not yep:
            c.execute("INSERT INTO projects (name, description, user_id) VALUES ('%s', '%s', '%s')" % (project.name,
                                                                                                       project.description,
                                                                                                       user.user_id))
            conn.commit()
            c.execute("SELECT id FROM projects WHERE name==('%s')" % project.name)
            id = c.fetchone()
            print(user.user_id)
            c.execute("INSERT INTO user_project (user_id, project_id) VALUES ('%d', '%d')" % (user.user_id, id[0]))
            conn.commit()
            conn.close()
        else:
            print("Adding error!")

    @classmethod
    #get current project from db
    def get_current_project(cls):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT current_id FROM current WHERE id==('%d')" % 2)
        project_id = c.fetchone()[0]
        print(project_id)
        if project_id == 0:
            return 1
        c.execute("SELECT * FROM projects WHERE id==('%d')" % project_id)
        project_data = c.fetchone()
        print("Test check")
        print(project_data)
        project = Project(project_data[1], project_data[2], project_data[3], None, project_data[0])
        return project

    #set current project, which take this method
    def _select_with_object(self):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("UPDATE current SET current_id=('%d') WHERE id==('%d')" % (self.id, 2))
        conn.commit()



#TASK WORK WITH DB
class TaskStorage:
    @classmethod
    #add task to db
    def add_task_to_db(cls, task):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("INSERT INTO tasks (name, desc, project_id, user_id, first_date, second_date, "
                  "tags, priority, archive, periodical) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s',"
                  "'%s','%s')" % (task.name, task.desc, task.project_id, task.user_id,
                                  task.first_date, task.second_date, task.tags, task.priority, 0, 0))
        conn.commit()
        conn.close()

    @classmethod
    #get projects names from db
    def get_all_projects(cls, column):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT name FROM tasks WHERE column_id=('%s')" % column.id)
        data = c.fetchall()
        return data




#USER WORK WITH DB
class UserStorage:
    @classmethod
    # add user to db
    def add_user_to_db(cls, user):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT username FROM users")
        all_usernames = c.fetchall()
        for i in all_usernames:
            if user.username == i[0]:
                print("User with that username has alredy registered!!!")
        print(all_usernames)
        c.execute(
            "INSERT INTO users (username, password, email, token) VALUES ('%s', '%s', '%s', '%s')" % (
            user.username, user.password
            , user.email, user.token))
        conn.commit()
        conn.close()

    @classmethod
    # get current user from db
    def get_current_user(cls):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT current_id FROM current WHERE id==1")
        id = c.fetchone()[0]
        print(id)
        c.execute("SELECT * FROM users WHERE id==('%d')" % id)
        data = c.fetchone()
        user = User(data[1], data[2], data[3], data[4], data[0])
        return user

    @classmethod
    #set current user
    def set_current_user(cls, username):
        conn = sqlite3.connect('database.sqlite3')
        c = conn.cursor()
        c.execute("SELECT id FROM users WHERE username==('%s') " % username)
        data = c.fetchone()
        id = data[0]
        c.execute("UPDATE current SET current_id=('%d') WHERE id==('%d')" % (id, 1))
        conn.commit()
        conn.close()