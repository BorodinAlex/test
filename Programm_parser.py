
from enum import Enum
from Controller import *


#categories for parsing
class Categories(Enum):
    user = 0
    project = 1
    task = 3


#subcategories for parsing
class SubCategories(Enum):
    add = 0

#set category
def set_category(arg):
    return Categories[arg]

#set subcategory
def set_subcategory(arg):
    return SubCategories[arg]


#main parse
def parse(args):
    print(args)
    category = set_category(args[0])
    subcategory = set_subcategory(args[1])
    if category == Categories.project:
        parse_project(subcategory, args[2:])
    if category == Categories.task:
        parse_task(subcategory, args[2:])

#project parse
def parse_project(subcategory, args):
    print(args)
    print(subcategory)
    if subcategory == SubCategories.add:
        project = ProjectController.create(args[0], args[1])

#task parse
def parse_task(subcategory, args):
    if subcategory == SubCategories.add:
        TaskController.add_task(args[0], args[1], args[2], args[3], args[4], args[5])


