from Models import Project, Task
from Storage import ProjectStorage, UserStorage, TaskStorage

#project add
class ProjectController:
    @classmethod
    def create(cls, name, description, token=None):
            user = UserStorage.get_current_user()
            memberlist = [user, ]
            if user:
                project = Project(name, description, user, memberlist)
                ProjectStorage.add_project_to_db(project, user)
            return project
#task add
class TaskController:
    @classmethod
    def add_task(cls, name, desc, first_date, second_date, tags, priority):
        project = ProjectStorage.get_current_project()
        user = UserStorage.get_current_user()
        task = Task(name, desc, project.id, user.user_id, first_date, second_date, tags, priority, 0,
                    0, 0)
        TaskStorage.add_task_to_db(task)

#user reg and log
#...